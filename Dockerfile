FROM pahud/openresty:latest

MAINTAINER Pahud "pahudnet@gmail.com"

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update && apt-get -y upgrade

ADD ./nginx.conf.d/nginx.conf /opt/openresty/nginx/conf/nginx.conf
ADD ./nginx.conf.d/cb.conf /opt/openresty/nginx/conf/sites-enabled.d/default.conf
ADD ./lua /opt/openresty/nginx/conf/lua
#ADD ./lualib /opt/openresty/nginx/conf/lualib
ADD ./supervisor.d/nginx.conf /etc/supervisor/conf.d/nginx.conf

RUN ln -sf /dev/stdout /opt/openresty/nginx/logs/api_access.log
RUN ln -sf /dev/stderr /opt/openresty/nginx/logs/error.log

WORKDIR /opt/openresty/nginx/conf
