local cjson            = require "cjson"
local cjson_safe       = require "cjson.safe"
local http             = require "resty.http"
local httpc            = http.new()
--local config         = ngx.shared.globalconfig;
local reqs             = {}
local resp             = {status="ok", reason="ok"}
ngx.req.read_body()
local body_data        = ngx.req.get_body_data();
local req_args         = ngx.req.get_uri_args()
ngx.ctx.req_args       = req_args

local final_endpoint   = ngx.var.final_endpoint

ngx.log(ngx.ERR, 'DEBUG - final_endpoint=', final_endpoint)

ngx.log(ngx.ERR, "got runscope callback payload body_data=", body_data )
local req_body=cjson_safe.decode(body_data)

if not req_body then
	ngx.log(ngx.ERR, '[ERROR] failed to parse request body into JSON')
	ngx.say('failed to parse request body into JSON')
	return
end

ngx.ctx.body_data = cjson.encode(req_body)

local function build_req_uri_args_from_table( t )
	_z = {}
	for k,v in pairs(t) do 
		table.insert( _z, k..'='..v)
	end 
	return table.concat( _z, '&')
end

local function post_to_final_endpoint( body_args_table )
	local res, err = httpc:request_uri( final_endpoint, 
	  {
		method = 'POST', 
		body = build_req_uri_args_from_table( body_args_table ),
		ssl_verify = false,
        headers = {
          ["Content-Type"] = "application/x-www-form-urlencoded",
         }		
	  } )
	return res, err
end



if req_body.result == 'pass' then
	ngx.log(ngx.ERR, '[OK]result=pass')
	return
end

local trigger_url = req_body.trigger_url
local ini_vars = req_body.initial_variables or {}
local has_retried = ini_vars.has_retried or 0
local max_retry = req_args.max_retry or 3
if not tonumber(max_retry) or tonumber(max_retry) > 5 or tonumber(max_retry) < 0 then
	max_retry = 3
end
ngx.log(ngx.ERR, '[INFO] max_retry set to ', max_retry)

--if req_body.result ~= 'pass' then
if tonumber(has_retried) < tonumber(max_retry) then
	ngx.log(ngx.ERR, "[INFO] start retry=", tostring(has_retried+1))
	ngx.log(ngx.ERR, "[INFO] requesting trigger_url: ", trigger_url..'?has_retried='..tostring(has_retried+1))
	res, err = httpc:request_uri(trigger_url..'?has_retried='..tostring(has_retried+1), { ssl_verify=false } )
	if err then
	  ngx.log(ngx.ERR, "[ERR] got err: ", err)
	else
	  ngx.log(ngx.ERR, "[INFO] trigger DONE")
	  ngx.log(ngx.ERR, res.body )
	  ngx.print( res.body )
	end
else
	ngx.log(ngx.ERR, '[INFO] max_retry reached' )
	-- callback to final_endpoint
	if final_endpoint then
		ngx.log(ngx.ERR, "start posting to final_endpoint: ", final_endpoint)
		local final_endpoint_post_args = req_args
		final_endpoint_post_args.title = req_body.bucket_name..' - '..req_body.test_name..' - '..req_body.result
		final_endpoint_post_args.date_time = ngx.localtime()
		--final_endpoint_post_args.source = 'runscope'
		-- 如果是runscope的測試，則把runscope payload裡面 test_id 當做MayDay的ueid(unique event identifier)
		if req_args.source == "runscope" then
			final_endpoint_post_args.ueid = req_body.test_id
		end
		final_endpoint_post_args.content = ngx.escape_uri( cjson.encode( req_body ) )
		res, err = post_to_final_endpoint( final_endpoint_post_args )
		ngx.log(ngx.ERR, "[FINEL ENDPONIT] final_endpoint post args: ", cjson.encode(final_endpoint_post_args) )
		if res then
			ngx.log(ngx.ERR, '[FINEL ENDPONIT] res.body=', res.body, ' and res.status=', res.status )
		end
		ngx.log(ngx.ERR, 'err=', err)
		ngx.log(ngx.ERR, "final_endpoint posted")
	end

end



--end
